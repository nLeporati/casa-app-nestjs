import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { BoxModule } from './box/box.module';
import { GroupsModule } from './groups/groups.module';
import { ChargesModule } from './charges/charges.module';
import { AuthModule } from './auth/auth.module';
import { AccountsModule } from './accounts/accounts.module';
require('dotenv').config()

@Module({
  imports: [
    MongooseModule.forRoot(process.env.DB, { useUnifiedTopology: true, useNewUrlParser: true }),
    UsersModule,
    BoxModule,
    GroupsModule,
    ChargesModule,
    AuthModule,
    AccountsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
