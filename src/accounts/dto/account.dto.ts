export class AccountDto {
  readonly user: string;
  readonly username: string;
  readonly password: string;
  readonly activeGroup?: string;
}