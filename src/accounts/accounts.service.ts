import { Injectable, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AccountDto } from './dto/account.dto';
import { Account } from './interfaces/account.interface';

@Injectable()
export class AccountsService {
  constructor(@InjectModel('Account') private readonly accountModel: Model<Account>) {}

  async getOne(username: string): Promise<Account> {
    const account = await this.accountModel.findOne({ username }).populate('user').exec();
    if (!account) throw new UnauthorizedException();
    return account;
  }

  async create(account: AccountDto): Promise<Account> {
    return await this.accountModel.create(account);
  }

  async updateActiveGroup(accountId: string, groupId: string) : Promise<Account> {
    const account = await this.accountModel.findById(accountId);
    if (!account) throw new NotFoundException();

    account.activeGroup = groupId;

    return await account.save();
  }
}
