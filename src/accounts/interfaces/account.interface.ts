import { Document } from "mongoose";
import { User } from "src/users/interfaces/user.interface";

export interface Account extends Document{
  user: User,
  username: string,
  password: string,
  activeGroup: string;
}