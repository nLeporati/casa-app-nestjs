import * as mongoose from 'mongoose';

export const AccountSchema = new mongoose.Schema({
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  username: { type: String, required: true },
  password: { type: String, required: true },
  activeGroup: {type: mongoose.Schema.Types.ObjectId, ref: 'Group'},
}, {timestamps: true});
