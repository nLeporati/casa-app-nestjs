import * as mongoose from 'mongoose';

export const ChargeSchema = new mongoose.Schema({
  description: { type: String, required: true },
  amount: { type: Number, required: true, min: 1 },
  payer: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  box: { type: mongoose.Schema.Types.ObjectId, ref: 'Box', required: true },
}, { timestamps: true });
