import { Module } from '@nestjs/common';
import { ChargesController } from './charges.controller';
import { ChargesService } from './charges.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ChargeSchema } from './schemas/charge.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Charge', schema: ChargeSchema },
    ]),
  ],
  controllers: [ChargesController],
  providers: [ChargesService]
})
export class ChargesModule {}
