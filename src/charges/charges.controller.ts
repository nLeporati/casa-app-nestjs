import { Controller, Post, Body, Delete, Param, Put, UseGuards } from '@nestjs/common';
import { ChargesService } from './charges.service';
import { ChargeDto } from './dto/charge.dto';
import { Charge } from './interfaces/charge.interface';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@Controller('charges')
@UseGuards(JwtAuthGuard)
export class ChargesController {
  constructor(private readonly chargesService: ChargesService) {}

  @Post()
  async create(@Body() chargeDto: ChargeDto): Promise<Charge> {
    return await this.chargesService.create(chargeDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() chargeDto: ChargeDto): Promise<Charge> {
    return this.chargesService.update(id, chargeDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    await this.chargesService.delete(id);
  }
}
