export class ChargeDto {
  description: string;
  amount: number;
  payer: string;
  box: string;
};
