import { Document } from "mongoose";
import { User } from "src/users/interfaces/user.interface";
import { Box } from "../../box/interfaces/box.interface";

export interface Charge extends Document {
  description: string,
  amount: number,
  payer: User['_id'],
  box: Box['_id']
};
