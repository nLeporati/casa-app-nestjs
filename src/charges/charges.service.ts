import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { Charge } from './interfaces/charge.interface';
import { InjectModel } from '@nestjs/mongoose';
import { ChargeDto } from './dto/charge.dto';

@Injectable()
export class ChargesService {
  constructor(
    @InjectModel('Charge') private readonly chargeModel: Model<Charge>
  ) {}

  async create(chargeDto: ChargeDto): Promise<Charge> {
    const charge = new this.chargeModel(chargeDto);
    return charge.save();
  }

  async delete(id: string): Promise<void> {
    const res = await this.chargeModel.findByIdAndDelete(id);
    if (!res) throw new NotFoundException();
  }

  async update(id: string, chargeDto: ChargeDto): Promise<Charge> {
    const res = await this.chargeModel.findByIdAndUpdate(id, chargeDto, { timestamps: true, new: true });
    if (!res) throw new NotFoundException();
    return res;
  }
}
