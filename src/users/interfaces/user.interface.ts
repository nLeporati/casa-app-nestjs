import { Document } from "mongoose";


export interface User extends Document {
  name: string;
  avatarText: string;
  groups: string[],
  createdAt: string,
  updatedAt: string,
}