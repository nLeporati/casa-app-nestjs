import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async getOne(id: string): Promise<User> {
    const user = await this.userModel.findById(id);
    if (!user) throw new NotFoundException();
    return user;
  }

  async getOneByUsername(username: string): Promise<User> {
    const user = await this.userModel.findOne({name: username});
    if (!user) throw new NotFoundException();
    return user;
  }

  async create(userDto: UserDto): Promise<User> {
    const user = new this.userModel(userDto);
    user.avatarText = this.getAvatarText(user);
    
    return user.save();
  }

  async update(id: string, userDto: UserDto): Promise<User> {
    const user = await this.userModel.findByIdAndUpdate(id, userDto, { timestamps: true, new: true });
    if (!user) throw new NotFoundException();

    user.avatarText = this.getAvatarText(user);
    return user;
  }

  async delete(id: string): Promise<void> {
    const res = await this.userModel.findByIdAndDelete(id);
    if (!res) throw new NotFoundException();
  }

  private getAvatarText(user: User): string {
    let secondLetter: string;
    const words = user.name.split(' ');
    secondLetter = (words.length > 1) ? secondLetter = words[1][0].toUpperCase() : user.name[1].toUpperCase();
    return user.name[0].toUpperCase() + secondLetter;
  }
}
