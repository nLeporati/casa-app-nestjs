import { Controller, Post, HttpCode, HttpStatus, Body, Get, Param, Delete, Put, Request, UseGuards } from '@nestjs/common';
import { BoxService } from './box.service';
import { BoxDto } from './dto/box.dto';
import { Box } from './interfaces/box.interface';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';  
import { Collect } from './interfaces/collect.interface';

@Controller('box')
@UseGuards(JwtAuthGuard)
export class BoxController {
  constructor(private readonly boxService: BoxService) {}

  @Get(':id')
  async get(@Param('id') id: string): Promise<Box> {
    return await this.boxService.getById(id);
  }

  @Get('historical/:groupId')
  async getHistorical(@Param('groupId') groupId: string): Promise<Box[]> {
    return await this.boxService.getHistorical(groupId);
  }

  @Post()
  async create(@Body() boxDto: BoxDto, @Request() req): Promise<Box> {
    return await this.boxService.create(boxDto, req.user);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() boxDto: BoxDto): Promise<Box> {
    return this.boxService.update(id, boxDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    this.boxService.delete(id);
  }

  @Get(':id/collect')
  async collect(@Param('id') id: string): Promise<Collect[]> {
    return this.boxService.collect(id);
  }

  @Get(':id/collect/confirm')
  @HttpCode(HttpStatus.OK)
  async confirmCollect(@Param('id') id: string): Promise<void> {
    await this.boxService.confirmCollect(id); 
  }
}
