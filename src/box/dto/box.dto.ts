export class BoxDto {
  readonly name: string;
  readonly group: string;
  readonly createdBy: string;
  // readonly charges?: string[];
  // readonly collection: [{type: mongoose.Schema.Types.ObjectId, ref: 'Collection'}]
}