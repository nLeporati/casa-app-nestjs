import * as mongoose from 'mongoose';
import { UserSchema } from 'src/users/schemas/user.schema';

const PaymentSchema = new mongoose.Schema({
    user: {type: UserSchema, required: true}, 
    amount: { type: Number, required: true}
});

export default PaymentSchema;