import * as mongoose from 'mongoose';
import PaymentSchema from './payment.schema';
import { UserSchema } from 'src/users/schemas/user.schema';

export const CollectSchema = new mongoose.Schema({
  totalAmount: Number,
  collector: {type: UserSchema},
  payments: [{type: PaymentSchema}],
});
