import * as mongoose from 'mongoose';
import { UserSchema } from 'src/users/schemas/user.schema';
import PaymentSchema from './payment.schema';

export const BoxSchema = new mongoose.Schema({
  name: { type: String, required: true },
  totalAmount: { type: Number },
  group: {type: mongoose.Schema.Types.ObjectId, ref: 'Group'},
  createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  collect: [{
    collector: {type: UserSchema, required: true},
    totalAmount: Number,
    payments: [{type: PaymentSchema}],
  }],
  closedAt: { type: mongoose.Schema.Types.Date },
}, { timestamps: true, toJSON: {virtuals: true} });

BoxSchema.virtual('charges', {
  ref: 'Charge',
  localField: '_id',
  foreignField: 'box', 
});

export default BoxSchema;
