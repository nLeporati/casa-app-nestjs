import { Payment } from "./payment.interface";
import { User } from "../../users/interfaces/user.interface";

export interface Collect {
  collector: User,
  totalAmount: number,
  payments: Payment[]
};
