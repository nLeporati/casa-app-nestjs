import { Document } from "mongoose";
import { Group } from "src/groups/interfaces/group.interface";
import { Charge } from "src/charges/interfaces/charge.interface";
import { Collect } from "./collect.interface";

export interface Box extends Document {
  totalAmount: number,
  name: string,
  createdBy: string,
  group: string | Group,
  collect: Collect[],
  charges: Charge[],
  closedAt?: string,
}