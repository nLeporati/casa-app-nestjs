import { User } from "../../users/interfaces/user.interface";

export interface Payment {
  user: User, 
  amount: number
};
