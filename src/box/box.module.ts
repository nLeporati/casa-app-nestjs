import { Module } from '@nestjs/common';
import { BoxController } from './box.controller';
import { BoxService } from './box.service';
import { MongooseModule } from '@nestjs/mongoose';
import BoxSchema from './schemas/box.schema';
import { UserSchema } from 'src/users/schemas/user.schema';
import { ChargeSchema } from 'src/charges/schemas/charge.schema';
import { GroupsModule } from 'src/groups/groups.module';

@Module({
  imports: [
    GroupsModule,
    MongooseModule.forFeature([
      { name: 'Box', schema: BoxSchema },
      { name: 'Charge', schema: ChargeSchema },
      { name: 'User', schema: UserSchema }
    ]),
  ],
  controllers: [BoxController],
  providers: [BoxService]
})
export class BoxModule {}
