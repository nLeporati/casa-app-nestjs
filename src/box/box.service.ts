import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Box } from './interfaces/box.interface';
import { BoxDto } from './dto/box.dto';
import { Group } from 'src/groups/interfaces/group.interface';
import { User } from 'src/users/interfaces/user.interface';
import { AuthUser } from 'src/auth/interfaces/auth-user.interface';
import { Collect } from './interfaces/collect.interface';
import { GroupsService } from 'src/groups/groups.service';

@Injectable()
export class BoxService {
  constructor(
    private readonly groupsService: GroupsService,
    @InjectModel('Box') private readonly boxModel: Model<Box>,
  ) {}

  async getById(id: string) {
    const box = await this.boxModel.findById(id).populate('createdBy').populate({path: 'charges', populate: 'payer'}).exec();
    if (!box) throw new NotFoundException();
    
    if (box.charges.length > 0) {
      box.totalAmount = box.charges.map(charge => charge.amount).reduce((total, amount) => total += amount);
    } else {
      box.totalAmount = 0;
      box.charges = [];
    }
    return box;
  }

  async getHistorical(groupId: string): Promise<Box[]> {
    const boxes = await this.boxModel.find({group: groupId, closedAt: {$ne: null}}).select('-collect').exec();
    if (!boxes) throw new NotFoundException();

    return boxes;
  }
  
  async create(boxDto: BoxDto, user: AuthUser): Promise<Box> {
    const box = new this.boxModel(boxDto);
    box.totalAmount = 0;
    const newBox = await box.save();
    this.groupsService.updateActiveBox(newBox.group as string, newBox._id);
    return newBox;
  }

  async update(id: string, boxDto: BoxDto): Promise<Box> {
    const res = await this.boxModel.findByIdAndUpdate(id, boxDto, { timestamps: true, new: true });
    if (!res) throw new NotFoundException();
    return res;
  }

  async delete(id: string): Promise<void> {
    const res = await this.boxModel.findByIdAndDelete(id);
    if (!res) throw new NotFoundException();
  }

  async confirmCollect(id: string): Promise<void> {
    const box = await this.boxModel.findById(id).populate('charges').exec();
    if (!box) throw new NotFoundException();
    if (box.closedAt) throw new BadRequestException('Box already closed');

    box.closedAt = new Date().toISOString();
    box.collect = await this.collect(id);

    if (box.charges.length > 0) {
      box.totalAmount = box.charges.map(charge => charge.amount).reduce((total, amount) => total += amount);
    } else {
      box.totalAmount = 0;
    }

    await box.updateOne(box, { timestamps: true });
    await this.groupsService.updateActiveBox(box.group as string, undefined);
  }

  async collect(boxId: string): Promise<Collect[]> {
    /* Obtener grupo con usuarios y cobros
    */
    const box = await this.boxModel.findById(boxId)
      .populate([
        { path: 'group', populate: 'users'},
        { path: 'charges'}])
      .exec();

    if (!box) throw new NotFoundException();

    const collects: Collect[] = [];
    const group = box.group as Group;
    const groupUsers = group.users as User[];

    /* Se calcula el pago total que realizo cada persona
    ** y se divide por la cantidad de personas del grupo
    */
   groupUsers.forEach((user: User) => {
      const charges = box.charges.filter(charge => charge.payer == user.id);

      let total = 0;
      if (charges.length > 0) {
        total = charges.map(charge => charge.amount).reduce((totalA, amount) => totalA += amount);
      }

      collects.push({
        collector: user,
        totalAmount: total,
        payments: []
      });
    });

    /* Se restan los totales (divididos entre el número de usuarios) entre usuarios
    */
   collects.forEach(collect => {
      const others = groupUsers.filter(user => user._id != collect.collector._id);

      const collectAmount = Math.floor(collect.totalAmount / groupUsers.length);
      let payAmount = 0;

      others.forEach(user => {
        const otherCollect = collects.find(col => col.collector._id == user._id);
        payAmount = collectAmount - Math.floor(otherCollect.totalAmount / groupUsers.length);

        if (payAmount > 0) {
          otherCollect.payments.push({user: collect.collector, amount: payAmount});
        }
      });
   });

    return collects;
  }

}
