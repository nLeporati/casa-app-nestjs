export interface AuthUser {
  readonly username: string,
  readonly userId: string
}