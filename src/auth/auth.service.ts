import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AccountsService } from 'src/accounts/accounts.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly accountsService: AccountsService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const account = await this.accountsService.getOne(username);
    if (account && account.password == pass) {
      return account;
    }
    return null;
  }

  async login(account: any): Promise<any> {
    const payload = { 
      id: account.id,
      username: account.username,
      user: account.user
    };
    return {
      // eslint-disable-next-line @typescript-eslint/camelcase
      access_token: this.jwtService.sign(payload),
    };
  }
}
