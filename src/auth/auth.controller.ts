import { Controller, Post, UseGuards, Request, Body, Get, Param } from '@nestjs/common';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { AccountDto } from '../accounts/dto/account.dto';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { AccountsService } from 'src/accounts/accounts.service';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly accountsService: AccountsService,
    private readonly authService: AuthService
  ) {}

  @Post('login')
  @UseGuards(LocalAuthGuard)
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @Post('signup')
  async signup(@Body() account: AccountDto) {
    return await this.accountsService.create(account);
  }

  @Get(':id/change-group/:groupId')
  async updateActiveGroup(@Param('id') accountId: string, @Param('groupId') groupId: string): Promise<void> {
    await this.accountsService.updateActiveGroup(accountId, groupId);
  }
}
