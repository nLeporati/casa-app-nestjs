import { Controller, Get, Param, Post, Body, Put, Delete, UseGuards } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { Group } from './interfaces/group.interface';
import { GroupDto } from './dto/group.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { JoinGroupDto } from './dto/join-group.dto';

@Controller('groups')
@UseGuards(JwtAuthGuard)
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @Get(':id')
  async get(@Param('id') id: string): Promise<Group> {
    return await this.groupsService.getById(id);
  }

  @Get('user/:id')
  async getByUser(@Param('id') id: string): Promise<Group[]> {
    return await this.groupsService.getByUser(id);
  }

  @Post()
  async create(@Body() groupDto: GroupDto): Promise<Group> {
    return await this.groupsService.create(groupDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() groupDto: GroupDto): Promise<Group> {
    return this.groupsService.update(id, groupDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    await this.groupsService.delete(id);
  }

  @Post('join')
  async join(@Body() joinGroupDto: JoinGroupDto): Promise<Group> {
    return await this.groupsService.join(joinGroupDto);
  }

  @Put(':groupId/users/:userId')
  async addUser(@Param('groupId') groupId: string, @Param('userId') userId: string): Promise<void> {
    await this.groupsService.addUser(groupId, userId);
  }

  @Delete(':groupId/users/:userId')
  async removeUser(@Param('groupId') groupId: string, @Param('userId') userId: string): Promise<void> {
    await this.groupsService.removeUser(groupId, userId);
  }

  @Post(':groupId/users/:userId/quit')
  async quit(@Param('groupId') groupId: string, @Param('userId') userId: string): Promise<Group> {
    return await this.groupsService.quit(groupId, userId);
  }

}
