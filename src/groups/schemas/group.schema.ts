import * as mongoose from 'mongoose';

const GroupSchema = new mongoose.Schema({
  name: { type: String, required: true },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  activeBox: { type: mongoose.Schema.Types.ObjectId, ref: 'Box' },
  inviteCode: { type: String },
}, {timestamps: true });

/*
GroupSchema.virtual('boxes', {
  ref: 'Box',
  localField: '_id',
  foreignField: 'group',
  options: {sort: { createdAt: -1 }},  
});
*/

export default GroupSchema;
