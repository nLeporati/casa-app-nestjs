import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Group } from './interfaces/group.interface';
import { GroupDto } from './dto/group.dto';
import { User } from 'src/users/interfaces/user.interface';
import { JoinGroupDto } from './dto/join-group.dto';
import * as crypto from 'crypto-random-string';
import * as mongoose from 'mongoose';

@Injectable()
export class GroupsService {
  constructor(
    @InjectModel('Group') private readonly groupModel: Model<Group>,
    @InjectModel('User') private readonly userModel: Model<User>
  ) {}

  async getById(id: string): Promise<Group> {
    const res = await this.groupModel.findById(id).populate('users').exec();
    if (!res) throw new NotFoundException();
    return res;
  }

  async getByUser(userId: string): Promise<Group[]> {
    // const res = await this.groupModel.find({createdBy: userId}, '-users')
    //   .catch(() => {throw new NotFoundException()});
    const res = await this.userModel.findById(userId).populate('groups').exec();
    if (!res) throw new NotFoundException();
    const groups = res.groups as unknown as Group[];
    return groups;
  }

  async create(groupDto: GroupDto): Promise<Group> {
    const group = new this.groupModel(groupDto);
    group.users.push(group.createdBy);
    const id: string = group.id;
    group.inviteCode = (id.slice(id.length-4, id.length) + crypto({length: 4, type: 'hex'})).toUpperCase();

    const user = await this.userModel.findById(group.createdBy);
    user.groups.push(group._id);
    await user.save();

    return await group.save();
  }

  async update(id:string, groupDto: GroupDto): Promise<Group> {
    const res = await this.groupModel.findByIdAndUpdate(id, groupDto, { timestamps: true, new: true });
    if (!res) throw new NotFoundException();
    return res;
  }

  async updateActiveBox(id: string, activeBox: string): Promise<void> {
    await this.groupModel.findByIdAndUpdate(id, {activeBox}, {timestamps: true, new: true});
  }

  async join(joinDto: JoinGroupDto): Promise<Group> {
    const group = await this.groupModel.findOne({inviteCode: joinDto.inviteCode});
    if (!group) throw new BadRequestException('Bad invite code');

    const user = await this.userModel.findById(joinDto.user);
    if (!user) throw new BadRequestException('User not found');

    await this.addUser(group._id, user._id);

    user.groups.push(group._id);
    await user.save();

    return group;
  }

  async quit(groupId: string, userId: string): Promise<Group> {
    let group = await this.groupModel.findById(groupId);
    if (!group) throw new BadRequestException('Group not found');

    const user = await this.userModel.findById(userId);
    if (!user) throw new BadRequestException('User not found');

    group = await this.removeUser(group._id, user._id);
    
    user.groups = user.groups.filter(id => String(id) != String(groupId));
    await user.save();
    
    return group;
  }

  async delete(id: string): Promise<void> {
    const res = await this.groupModel.findByIdAndDelete(id);
    if (!res) throw new NotFoundException();
  }

  async addUser(groupId: string, userId: string): Promise<void> {
    const group = await this.groupModel.findById(groupId);
    if (!group) throw new NotFoundException("Group not found");
    
    const users = group.users as string[];
    users.push(userId);

    await group.save();
  }

  async removeUser(groupId: string, userId: string): Promise<Group> {
    const group = await this.groupModel.findById(groupId);
    if (!group) throw new NotFoundException();
    
    const users = group.users as string[];
    group.users = users.filter(id => String(id) != String(userId));

    return await group.save();
  }

}
