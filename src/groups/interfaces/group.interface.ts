import { Document } from "mongoose";
import { User } from "src/users/interfaces/user.interface";
import { Box } from "src/box/interfaces/box.interface";

export interface Group extends Document {
  name: string;
  users: User[] | string[];
  createdBy: User['_id'];
  activeBox: Box['_id'];
  inviteCode: string;
}