export class JoinGroupDto {
  readonly user: string;
  readonly inviteCode: string;
};
