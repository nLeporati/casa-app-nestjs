export class GroupDto {
  readonly name?: string;
  readonly createdBy?: string;
  readonly activeBox?: string;
}